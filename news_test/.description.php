<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => 'Список новостей с тегами',
	"DESCRIPTION" => 'Список новостей с тегами',
	"ICON" => "/images/lists_list.gif",
	"SORT" => 10,
	"CACHE_PATH" => "Y",
	"PATH" => array(
		"ID" => "MG",
	),
);

?>