<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
require_once(__DIR__ . '/class.php');

use Bitrix\Main\Localization\Loc;
use MG\Tags\TagTable;

define('STOP_STATISTICS', true);
define('NO_AGENT_STATISTIC', 'Y');
define('BX_SECURITY_SHOW_MESSAGE', true);


Loc::loadMessages(__FILE__);
$json = file_get_contents('php://input');
if ($json) $request = json_decode($json, true);

if (!$request['action']) return;

global $DB;

$result = ['error' => ''];

switch ($request['action']) {
    case 'saveTag':
        $request['name'] = trim($request['name']);

        $arFilter = [
            '=tag' => $DB->ForSql($request['name']),
            '=element_id' => $DB->ForSql($request['element_id'])
        ];
        if ((int)$request['id'] > 0) $arFilter['!=id'] = $DB->ForSql($request['id']);

        $res = TagTable::getList(['filter' => $arFilter, 'limit' => 1]);
        if ($res->fetch()) $result['error'] = GetMessage('CC_BLL_EXIST');

        if (!$result['error']) {
            if ((int)$request['id'] > 0) {
                $res = TagTable::update($DB->ForSql($request['id']), [
                    'tag' => $DB->ForSql($request['name'])
                ]);
            } else {
                $res = TagTable::add([
                    'tag' => $DB->ForSql($request['name']),
                    'element_id' => $DB->ForSql($request['element_id'])
                ]);
            }

            if ($res->isSuccess()) {
                if ((int)$request['id'] == 0) $result['insert_id'] = $res->getId();
                $result['status'] = 'ok';
                CBitrixComponent::clearComponentCache('mg:news_test');
            } else $result['error'] = GetMessage('CC_BLL_SOME_ERROR');
        }

        break;
    case 'deleteTag':
        $result['test'] = $OUTDB;
        if ((int)$request['id'] > 0) {
            $res = TagTable::delete($DB->ForSql($request['id']));
            if ($res->isSuccess()) {
                $result['status'] = 'ok';
                CBitrixComponent::clearComponentCache('mg:news_test');
            } else $result['error'] = GetMessage('CC_BLL_SOME_ERROR');
        } else $result['error'] = GetMessage('CC_BLL_SOME_ERROR');

        break;
}

echo json_encode($result);