<?php

namespace MG\Tags;

use Bitrix\Main\Entity;

class TagTable extends Entity\DataManager
{
    public static function getConnectionName()
    {
        return 'tag';
    }

    public static function getTableName()
    {
        return 'tags';
    }

    public static function getMap()
    {
        return [
            new Entity\IntegerField('id', [
                'primary' => true,
                'autocomplete' => true
            ]),
            new Entity\StringField('tag', [
                'required' => true,
            ]),
            new Entity\IntegerField('element_id', [
                'required' => true,
            ])
        ];
    }
}