<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arResult */
/** @var array $arParams */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @var CBitrixComponentTemplate $this */
/** @var CBitrixComponent $component */

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
?>
<section id="newsapp">
    <div class="container">
        <div class="row">
            <div class="col">
                <?if($arResult['ITEMS']){?>
                    <?foreach ($arResult['ITEMS'] as $item) {?>
                        <div class="mb-3">
                            <h3 class="mb-2"><?=$item['NAME']?></h3>
                            <div class="text-success">Теги:
                                <span class="btn btn-sm btn-primary m-2" @click="editTag('<?=$item['ID']?>',tag.id,tag.tag)" v-for="tag in tags[<?=$item['ID']?>]">{{ tag.tag }}</span>
                                <span class="btn btn-sm btn-primary" @click="editTag('<?=$item['ID']?>')">+</span>
                            </div>
                        </div>
                    <?}?>
                    <?if($arResult["NAV_STRING"]){?>
                        <div class="my-5">
                            <?=$arResult["NAV_STRING"]?>
                        </div>
                    <?}?>
                <?} else {?>
                    <p><?=GetMessage('CT_BLL_NOT_NEWS')?></p>
                <?}?>
            </div>
        </div>
    </div>
    <div class="modal-mask" v-if="showModal" @close="showModal = false">
        <div class="modal-wrapper">
            <div class="modal-container">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <template v-if="currentTagID"><?=GetMessage('CT_BLL_EDIT_TAG')?></template>
                        <template v-else><?=GetMessage('CT_BLL_ADD_TAG')?></template>
                    </h5>
                    <button type="button" class="btn-close" @click="showModal = false"></button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="mb-3">
                            <input type="text" class="form-control" id="recipient-name" v-model="currentTagName">
                        </div>
                        <div class="d-flex justify-content-between">
                            <button type="button" class="btn btn-primary" @click="saveTag()">
                                <template v-if="currentTagID"><?=GetMessage('CT_BLL_EDIT')?></template>
                                <template v-else><?=GetMessage('CT_BLL_ADD')?></template>
                            </button>
                            <button type="button" v-if="currentTagID" class="btn btn-danger" @click="deleteTag()"><?=GetMessage('CT_BLL_DELETE')?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>




<script type="text/javascript">
    Vue.prototype.$showModal = '123'
    var app = new Vue({
        el: '#newsapp',
        data: {
            currentTagID: 0,
            currentElementID: 0,
            showModal: false,
            currentTagName: '',
            url: '<?=$arResult["COMPONENT_PATH"]?>/ajax.php',
            tags: {
                <? $e=0; foreach ($arResult["TAGS"] as $ELEMENT_ID=>$TAGS) {$e++; $t=0; ?>
                        <?=$ELEMENT_ID?>: [
                            <? foreach ($TAGS as $TAG) { $t++; ?>
                                {id:<?=$TAG['id']?>,tag:'<?=$TAG['tag']?>',element_id:<?=$TAG['element_id']?>}<?=($t==count($TAGS)?'':',')?>
                            <?}?>
                        ]<?=($e==count($arResult["TAGS"])?'':',')?>
                <?}?>
            }
        },
        methods: {
            editTag(element_id,id,name) {
                if(id) {
                    this.currentTagID = id;
                    this.currentTagName=name;
                }
                else {
                    this.currentTagID = 0;
                    this.currentTagName='';
                }
                this.currentElementID = element_id;

                this.showModal = true;
            },
            saveTag() {
                if(this.currentTagName && this.currentElementID) {
                    var params = {
                        action: 'saveTag',
                        id: this.currentTagID,
                        element_id: this.currentElementID,
                        name: this.currentTagName
                    };
                    axios.post(this.url, params)
                        .then((response) => {
                            if (response.data && response.data.status && response.data.status == 'ok') {
                                if(!this.tags[this.currentElementID]) this.tags[this.currentElementID]=[];
                                if(response.data.insert_id) this.tags[this.currentElementID].push({id:response.data.insert_id,tag:this.currentTagName,element_id:this.currentElementID});
                                else if(this.currentTagID) {
                                    this.tags[this.currentElementID].map((value, key) => {
                                        if (value.id == this.currentTagID) {
                                            this.tags[this.currentElementID][key].tag=this.currentTagName;
                                            return true;
                                        }
                                    });
                                }
                                this.showModal = false;
                            } else if (response.data && response.data.error) {
                                alert(response.data.error);
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }
                else {
                    alert('<?=GetMessage('CT_BLL_TAG_NAME_EMPTY')?>');
                }
            },
            deleteTag() {
                if(this.currentElementID) {
                    var params = {
                        action: 'deleteTag',
                        id: this.currentTagID
                    };
                    axios.post(this.url, params)
                        .then((response) => {
                            if (response.data && response.data.status && response.data.status == 'ok') {
                                this.tags[this.currentElementID].map((value, key) => {
                                    if (value.id == this.currentTagID) {
                                        this.tags[this.currentElementID].splice(key, 1);
                                        return true;
                                    }
                                });
                                this.showModal = false;
                            } else if (response.data && response.data.error) {
                                alert(response.data.error);
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                }
                else {
                    alert('<?=GetMessage('CT_BLL_TAG_NAME_EMPTY')?>');
                }
            },
        }
    })
</script>
