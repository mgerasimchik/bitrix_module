<?
$MESS["CT_BLL_NOT_NEWS"] = "Новостей еще нет";
$MESS["CT_BLL_EDIT_TAG"] = "Изменить тэг";
$MESS["CT_BLL_ADD_TAG"] = "Добавить тэг";
$MESS["CT_BLL_EDIT"] = "Изменить";
$MESS["CT_BLL_ADD"] = "Добавить";
$MESS["CT_BLL_DELETE"] = "Удалить";
$MESS["CT_BLL_TAG_NAME_EMPTY"] = "Укажите тэг";