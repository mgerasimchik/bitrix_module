<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentName */
/** @var string $componentPath */
/** @var string $componentTemplate */
/** @var string $parentComponentName */
/** @var string $parentComponentPath */
/** @var string $parentComponentTemplate */
$this->setFrameMode(false);

use Bitrix\Main\Page\Asset;

global $DB;

if (!CModule::IncludeModule('iblock')) {
    ShowError(GetMessage("CC_BLL_MODULE_NOT_INSTALLED"));
    return;
}

if (!$arParams['IBLOCK_TYPE'] OR !$arParams['IBLOCK_ID'] OR !$arParams['EXT_BASE_HOST'] OR !$arParams['EXT_BASE_LOGIN'] OR !$arParams['EXT_BASE_PASS'] OR !$arParams['EXT_BASE_NAME']) {
    ShowError(GetMessage("CC_BLL_NO_PARAMS"));
    return;
}

$arResult["COMPONENT_PATH"] = $this->GetPath();

if (!$arParams['NEWS_COUNT'] OR !ctype_digit($arParams['NEWS_COUNT'])) $arParams['NEWS_COUNT'] = 5;
$arResult["GRID_ID"] = "news_test_grid";
$grid_options = new Bitrix\Main\Grid\Options($arResult["GRID_ID"]);
$grid_params = $grid_options->GetNavParams(['nPageSize' => $arParams['NEWS_COUNT']]);
if ($this->StartResultCache(3600, CDBResult::NavStringForCache($arParams['NEWS_COUNT'], false))) {

    // Получаем список элементов
    $arResult["ITEMS"] = [];
    $arFilter = ["IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y"];
    $arSelect = ["ID", "NAME"];
    $rsElements = CIBlockElement::GetList([], $arFilter, false, $grid_params, $arSelect);
    $elementIDs = [];
    while ($arElement = $rsElements->Fetch()) {
        $arResult["ITEMS"][] = $arElement;
        $elementIDs[] = $arElement['ID'];
    }

    // Ищем теги
    $arResult["TAGS"] = [];
    $res = MG\Tags\TagTable::getList(['filter'  => ['=element_id'=>$elementIDs]]);
    while ($wtemp = $res->Fetch()) {
        $arResult["TAGS"][$wtemp['element_id']][] = $wtemp;
    }

    $rsElements->bShowAll = false;
    $arResult["NAV_OBJECT"] = $rsElements;
    $componentObject = null;
    $arResult["NAV_STRING"] = $arResult["NAV_OBJECT"]->getPageNavStringEx($componentObject, "", "", false, null, $grid_params);

    $this->IncludeComponentTemplate();

}

Asset::getInstance()->addCss("https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css");
Asset::getInstance()->addJs("https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js");
Asset::getInstance()->addJs("https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js");
Asset::getInstance()->addJs("https://unpkg.com/axios/dist/axios.min.js");
?>